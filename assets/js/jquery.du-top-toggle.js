/**
 * Apply classes to elements based on scroll location
 * @var element   dom element to add or remove class from
 * @var classes   classes to add/remove from element
 * @var trigger   int  [data-top-distance] of @element
 */
jQuery( function() {
  var win     = jQuery( window );
  jQuery( '[data-top-distance]' ).each( function( index ) {
    var element = jQuery( this );
    var classes = element.attr( 'data-top-class' );
    var trigger = element.attr( 'data-top-distance' );
    jQuery( window ).scroll( function() {
      var scroll = win.scrollTop();
      if ( scroll >= trigger ) {
        element.removeClass( classes );
      } else {
        element.addClass( classes );
      }
    });
  });
});