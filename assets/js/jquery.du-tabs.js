/**
 *  Simple Tabs Functionality
 *  
 *  Parent:   data-du-tabs
 *  Trigger:  data-du-tab-trigger="element"
 *  Tab:      data-du-tab="element"
 */

jQuery( 'body' ).on( 'click', '[data-du-tab-trigger]', function() {
  
  var useCSS = true; // If false, hide using jQuery

  var Triggered = jQuery( this ).attr( 'data-du-tab-trigger' );
  var currentTabs = jQuery( this ).closest( '[data-du-tabs]' );
  var allTabs = jQuery( '[data-du-tab]' );
  var allTriggers = jQuery( '[data-du-tab-trigger]' );

  // Disengage all tabs and triggers
  if ( useCSS ) {
    jQuery( currentTabs ).find( allTabs ).removeClass( 'active' );
  } else {
    jQuery( currentTabs ).find( allTabs ).hide();
  }
  jQuery( currentTabs ).find( allTriggers ).removeClass( 'active' );

  // Engage selected tabs and triggers
  jQuery( this ).addClass( 'active' );

  if ( useCSS ) {
    jQuery( currentTabs ).find( '[data-du-tab="' + Triggered + '"]' ).addClass( 'active' );
  } else {
    jQuery( currentTabs ).find( '[data-du-tab="' + Triggered + '"]' ).show();
  }

});