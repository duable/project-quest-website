/**
 *  Simple Dynamic Gallery
 *  
 *  Parent:         data-du-thumb-gallery
 *  Trigger:        data-du-thumb-gallery-image="image-url.jpg"
 *  Image Viewport: data-du-thumb-gallery-display
 */

jQuery( 'body' ).on( 'click', '[data-du-thumb-gallery-image]', function() {
  
  var currentImage = jQuery( this ).attr( 'data-du-thumb-gallery-image' );
  var currentGallery = jQuery( this ).closest( '[data-du-thumb-gallery]' );
  var allGalleries = jQuery( '[data-du-thumb-gallery]' );
  var allTriggers = jQuery( '[data-du-thumb-gallery-image]' );

  // Disengage all triggers
  jQuery( currentGallery ).find( allTriggers ).removeClass( 'active' );

  // Engage selected triggers
  jQuery( this ).addClass( 'active' );

  // Load image in display unit
  jQuery( currentGallery ).children( '[data-du-thumb-gallery-display]' ).css( 'background-image', 'url(' + currentImage + ')' );
  console.log( currentImage );

});