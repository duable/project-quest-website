<?php
/**
 * Standard Page Layout
 * 
 * @category   Page
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */

get_header(); ?>

<main id="body" class="single-page" role="main">

<?php 
    while ( have_posts() ) : the_post(); 
    
      the_content();

    endwhile; 
?>

</main>

<?php get_footer(); ?>