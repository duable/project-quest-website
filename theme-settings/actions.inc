<?php
namespace du;

/**
 * Add all WordPress actions here.
 */
class actions {

  function __construct() {
    # Custom actions for a/b testing.
    #if ( !is_admin() ) :
    # add_action( 'init', array( $this, 'audience_tracking' ) );
    # add_action( 'init', array( $this, 'variation_tracking' ) );
    #endif;
    # Hide short URL ad
    add_action( 'admin_head', function() {
      echo '<style>.kc_us_offer{display:none}</style>';
    });

    # Actions based on query variables
    add_action( 'init', array( $this, 'du_link_actions' ) );
    
    # Quick fix for top-bar pos. in customizer
    add_action( 'wp_head', array( $this, 'customizer_styles' ) );
    
    # Add custom redirections here
    add_action( 'template_redirect', array( $this, 'redirects' ) );
    
    # Elementor Locations
    add_action( 'elementor/theme/register_locations', array( $this, 'elementor_hello_theme_register_elementor_locations' ) );

    # Elementor Widgets
    add_action( 'init', array( $this, 'elementor_widgets' ) );

  }

  function elementor_widgets() {
    if ( is_plugin_active( 'elementor/elementor.php' ) ) :
   
      if ( !file_exists( DU_VENDOR_DIR . '/duable/du-elementor/widgets/' ) )
        return;
      foreach ( glob( DU_VENDOR_DIR . '/duable/du-elementor/widgets/*/*.php' ) as $file ) 
        include_once $file ;
      foreach ( glob( get_template_directory() . '/widgets/*/*.php' ) as $file ) 
        include_once $file ;

    endif;
  }

  // Register Elementor Locations
  function elementor_hello_theme_register_elementor_locations( $elementor_theme_manager ) {
    $elementor_theme_manager->register_all_core_location();
  }

  /**
   * Define the variation to show by id.
   * If no audience is set, redirect to
   * brand's primary site.
   * 
   * @return void
   * @author mohammad@duable.com
   */
  function audience_tracking() {
    $audience = ( 
      !empty( $_GET[ 'aud_id' ] ) ?
        $_GET[ 'aud_id' ] : null
    );

    # URL to send traffic to when audience is not set
    $main_website = 'http://';

    # No audience means go to main website
    if ( empty( $audience ) ) 
      header( 'Location: ' . $main_website );

    # Define Audience IDs for landing pages
    define( 'AUD_ID',  $audience );
  }

  /**
   * Define the variation to show by id.
   * 
   * @return void
   * @author mohammad@duable.com
   */
  function variation_tracking() {
    if ( empty( $_GET[ 'ver' ] ) ) :
      define( 'VARIATION_ID', '0' );
      return;
    else :
      define( 'VARIATION_ID', $_GET[ 'ver' ] );
    endif;
  }

  /**
   * Define redirects based on conditions
   * 
   * @return void
   * @author mohammad@duable.com
   */
  function redirects() {
    global $post;
    if ( !empty( $post ) ) :
      if ( !empty( get_post_meta( $post->ID, 'du_require_activation', true ) ) && !du_active_user() ) {
        include_once TEMPLATEPATH . '/blocker-activate.php';
        return;
      }
    endif;
  }

  /**
   * Misc actions based on url vars
   *
   * @return void
   * @author mohammad@duable.com
   **/
  function du_link_actions() {
    if ( empty( $_GET[ "du_action" ] ) ) return;
    switch ( $_GET[ 'du_action' ] ) {
      case 'logout' :
        $logout_redirect = home_url();
        $logout_redirect = apply_filters( 'du_logout_redirect', $logout_redirect );
        wp_logout();
        header( "Location: " . $logout_redirect );
        break;

      default :
        break;
    }
  }

  /**
   * Add styles if in customizer
   */
  function customizer_styles() {
    if ( isset( $_REQUEST[ 'wp_customize' ] ) ) : ?>
    <style>
      .top-bar{ position: absolute; }
    </style>
    <?php endif;
  }

} new actions;
