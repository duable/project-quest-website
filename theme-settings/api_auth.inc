<?php

/**
 * Add authentication variables here
 * 
 * Usage:
 * $credentials = du_api_auth::twitter();
 * echo $credentials->consumer_key;
 */

class du_api_auth {

  static function twitter() {
    $auth = new stdClass();
    $auth->username             = '';
    $auth->consumer_key         = '';
    $auth->consumer_secret      = '';
    $auth->access_token         = '';
    $auth->access_token_secret  = '';
    $auth->bearer_token         = '';
    return $auth;
  }

  static function instagram() {
    $auth = new stdClass();
    $auth->client_id  = '';
    $auth->token      = '';
    return $auth;
  }

  static function youtube() {
    $api_key = '';
    return $api_key;
  }

  static function facebook() {
    $auth = new stdClass();
    $auth->app_id = '';
    $auth->secret = '';
    return $auth;
  }

}
