<?php

namespace du;

class custom_post_types {

  function __construct() {
    //add_action( 'init', array( $this, 'post_types' ) );
  }

  static function post_types() {

    /**
     * Example Post Type
     */
    $post_type_labels = array(
        'name'                => 'Post Types',
        'singular_name'       => 'Post Type',
        'menu_name'           => 'Post Types',
        'parent_item_colon'   => 'Parent Post Type:',
        'all_items'           => 'All Post Types',
        'view_item'           => 'View Post Type',
        'add_new_item'        => 'Add Post Type',
        'add_new'             => 'New Post Type',
        'edit_item'           => 'Edit Post Type',
        'update_item'         => 'Update Post Type',
        'search_items'        => 'Search Post Types',
        'not_found'           => 'No Post Types found',
        'not_found_in_trash'  => 'No Post Types found in trash',
    );

    $post_type_rewrite = array(
        'slug'                => 'post_type',
        'with_front'          => false,
        'pages'               => false,
        'feeds'               => false,
    );

    $post_type_args = array(
        'label'               => 'Post Types',
        'description'         => 'Post Types',
        'labels'              => $post_type_labels,
        'supports'            => array( 'title', 'custom-fields', 'revisions' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5.12,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'rewrite'             => $post_type_rewrite,
    );
    register_post_type( 'post_types', $post_type_args );

  }

} new custom_post_types ;