<?php
/**
 * Custom theme shortcodes
 *
 * @package default
 * @author mohammad@duable.com
 **/
class du_shortcodes {

  function __construct() {
    add_shortcode( 'du_nav', array( $this, 'du_nav' ) );
    add_shortcode( 'custom_menu', array( $this, 'custom_menu' ) );
    add_shortcode( 'sub_nav', array( $this, 'sub_nav' ) );
    add_shortcode( 'bloomerang_form', array( $this, 'bloomerang_form' ) );
  }


  public function bloomerang_form( $atts, $content = null ) {
    return '<script src="https://s3-us-west-2.amazonaws.com/bloomerang-public-cdn/projectquest/.widget-js/2622464.js" type="text/javascript"></script>';
  }
  /**
   * Display main navigation
   *
   * @return void
   * @author mohammad@duable.com
   **/
  public function du_nav( $atts, $content = null ) {

    # Get logo
    if ( !empty( $atts[ 'logo' ] ) ) :
      $logo_url = $atts[ 'logo' ];
    else :
      $custom_logo_id = get_theme_mod( 'custom_logo' );
      $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
      if ( has_custom_logo() ) :
              $logo_url = esc_url( $logo[0] );
      else :
              $logo_url = du_site_asset( '/assets/images/logo.png' );
      endif;
    endif;

    # Get navigation
    if ( !empty( $atts[ 'menu' ] ) ) :
      $menu = $atts[ 'menu' ];
    else :
      $menu = 'header-navigation';
    endif;

?>

<section class="top-bar">
  <div class="container">

    <a class="main-logo" href="/">
      <img src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
    </a>

    <button toggle-nav class="hamburger-icon clear">
      <span>toggle menu</span>
    </button>
    <aside class="overlay" full-menu-overlay></aside>
    <section full-menu>
      <nav class="main clear">
        <?php 
          wp_nav_menu( 
              array( 
                'theme_location' => $menu, 
                'container' => false,
              ) 
          ); 
        ?>
      </nav>
    </section>
    
  </div>
</section>

<?php
  }
  /**
   * Display custom menu by theme_location
   *
   * @return void
   * @author mohammad@duable.com
   **/
  public function custom_menu( $atts, $content = null ) {
    return wp_nav_menu( 
      array( 
        'theme_location' => $atts[ 'menu' ], 
        'container' => false,
        'echo'      => false
      ) 
    );
  }

  /**
   * Display sub navigation of current page
   *
   * @return void
   * @author mohammad@duable.com
   **/
  public function sub_nav( $atts, $content = null ) {
    return du_sub_nav( '', false, array(), '' );
  }

  
} new du_shortcodes;
