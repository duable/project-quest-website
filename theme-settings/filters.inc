<?php
namespace du;

/**
 * Add all WordPress filters here.
 */
class filters {

  function __construct() {

    # User logout redirect
    add_filter( 'du_logout_redirect', array( $this, 'logout_redirect' ) );

    # Output different page titles for different page types
    add_filter( 'du_page_title', array( $this, 'du_page_title_types') );

    # Updated wp_title()
    add_filter( 'wp_title', array( $this, 'du_wp_title' ), 10, 2 );

    # Custom classes added to <body> tag
    add_filter( 'body_class' , array( $this, 'body_classes' ) );
    
    # Edit allowed upload types
    add_filter( 'upload_mimes', array( $this, 'custom_upload_mimes' ) );

    # Ability to hide gform field labels
    add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
  }

  /**
   * Logout Redirect
   *
   * @return void
   * @author 
   **/
  function logout_redirect( $redirect ) {
    $redirect = home_url( '?logout=true' );
    return $redirect;
  }

  /**
   * Custom du_page_title() return values based on where
   * we're pulling from.
   */
  function du_page_title_types( $main_headline ) {
    if ( is_category() )
      $main_headline = single_cat_title( '', false );

    return $main_headline;
  }

  /**
   * Let's use our custom du_page_title function to determine
   * the wp_title content
   */
  function du_wp_title( $title, $sep ) {
    global $post, $paged, $page;
    $title = du_page_title();
    if ( is_feed() )
      return $title;
    // Add the site name.
    $title .= ' ' . $sep . ' ' . get_bloginfo( 'name' );
    /* Add a page number if necessary. */
    if ( $paged >= 2 || $page >= 2 )
      $title = "$title $sep " . sprintf( __( 'Page %s', 'du_trust' ), max( $paged, $page ) );
    return $title;
  }

  /**
   * Manipulate <body> classes
   * 
   * @param  array $classes standard classes for this page
   * @return array classes to include
   */
  function body_classes( $classes ) {
    global $post;
    # Get front page's post id
    $front_page = get_option( 'page_on_front' );
    # If current page is not front page post id, let's add subpage to <body> the classes
    if ( !empty( $post ) && $post->ID != $front_page && is_page() )
      $classes[] = 'subpage';
    return $classes;
  }
  
  /**
   * Uploadable mime types
   * @param  array  $existing_mimes
   * @return array
   */
  public function custom_upload_mimes ( $existing_mimes=array() ) {
    $existing_mimes[ 'ico' ] = 'image/x-icon';
    return $existing_mimes;
  }

} new filters;