<?php

namespace du;

class woocommerce {

  function __construct() {
    /**
     * Optimize WooCommerce Scripts
     * https://gist.github.com/DevinWalker/7621777
     */
    add_action( 'wp_enqueue_scripts', 
      array( $this, 'child_manage_woocommerce_styles' ) , 99 );

    # Disable cart fragments
    add_action( 'wp_enqueue_scripts', 
      array( $this, 'dequeue_wc_fragments' ), 100 );
    
    # Declare WooCommerce Support
    add_action( 'after_setup_theme', 
      array( $this, 'woocommerce_support' ) );

    # Remove Woo page wrappers
    remove_action( 'woocommerce_before_main_content', 
      array( $this, 'woocommerce_output_content_wrapper', 10 ) );
    remove_action( 'woocommerce_after_main_content', array( $this, 'woocommerce_output_content_wrapper_end', 10 ) );

    # Add Custom page wrappers
    add_action( 'woocommerce_before_main_content', 
      array( $this, 'theme_wrapper_start' ), 10 );
    add_action( 'woocommerce_after_main_content', 
      array( $this, 'theme_wrapper_end' ), 10 );

    # Disable Woocommerce Styles
    add_filter( 'woocommerce_enqueue_styles', 
      '__return_empty_array' );
  }


  function woocommerce_support() {
    add_theme_support( 'woocommerce' );
  }

  /**
   * Dequeue wc-fragments
   * Removes synchronous wp-admin/ajax request that happened on every page load when cart is non-empty.
   * This script is not in use and should be safe to disable.
   */
  function dequeue_wc_fragments() {
    wp_dequeue_script( 'wc-cart-fragments' );
  }

  function theme_wrapper_start() {
?>

<main id="body" class="single-page product-page" role="main">
  <article class="main-content content copy">
    <div class="container">

<?php
  }

  function theme_wrapper_end() {
?>

    </div>
  </article>
</main>

<?php  
  }

  function child_manage_woocommerce_styles() {
    //remove generator meta tag
    if ( !empty( $GLOBALS['woocommerce'] ) )
      remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

    //first check that woo exists to prevent fatal errors
    if ( function_exists( 'is_woocommerce' ) ) {
      //dequeue scripts and styles
      if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
        wp_dequeue_style( 'woocommerce-layout-css' );
        wp_dequeue_style( 'woocommerce-smallscreen-css' );
        wp_dequeue_style( 'woocommerce-general-css' );
        wp_dequeue_style( 'woocommerce_frontend_styles' );
        wp_dequeue_style( 'woocommerce_fancybox_styles' );
        wp_dequeue_style( 'woocommerce_chosen_styles' );
        wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
        wp_dequeue_script( 'wc_price_slider' );
        wp_dequeue_script( 'wc-single-product' );
        wp_dequeue_script( 'wc-add-to-cart' );
        wp_dequeue_script( 'wc-cart-fragments' );
        wp_dequeue_script( 'wc-checkout' );
        wp_dequeue_script( 'wc-add-to-cart-variation' );
        wp_dequeue_script( 'wc-single-product' );
        wp_dequeue_script( 'wc-cart' );
        wp_dequeue_script( 'wc-chosen' );
        wp_dequeue_script( 'woocommerce' );
        wp_dequeue_script( 'prettyPhoto' );
        wp_dequeue_script( 'prettyPhoto-init' );
        wp_dequeue_script( 'jquery-blockui' );
        wp_dequeue_script( 'jquery-placeholder' );
        wp_dequeue_script( 'fancybox' );
        wp_dequeue_script( 'jqueryui' );
      }
    }
  }

} new woocommerce;