<?php

namespace du;

class custom_taxonomies {

  function __construct() {
       add_action( 'init', array( $this, 'taxonomies' ) );
  }
  
  static function taxonomies() {
    
    /* Add categories to pages */
    register_taxonomy_for_object_type( 'category', 'page' );

    /**
     * Module Types Taxonomy
     */
    $labels = array(
        'name'                       => 'Module Type',
        'singular_name'              => 'Module Type',
        'menu_name'                  => 'Module Types',
        'all_items'                  => 'All Module Types',
        'parent_item'                => 'Module Type',
        'parent_item_colon'          => 'Module Type:',
        'new_item_name'              => 'New Module Type',
        'add_new_item'               => 'Add New Module Type',
        'edit_item'                  => 'Edit Module Type',
        'update_item'                => 'Update Module Type',
        'separate_items_with_commas' => 'Separate Module Types with Commas',
        'search_items'               => 'Search Module Types',
        'add_or_remove_items'        => 'Add or Remove Module Types',
        'choose_from_most_used'      => 'Choose from the most used Module Types',
    );
    $rewrite = array(
        'slug'                       => 'module-types',
        'with_front'                 => false,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
    );
    //register_taxonomy( 'du_mods_types', array( 'du_mods' ), $args );
    
    /**
     * FAQs Categories
     */
    $labels = array(
        'name'                       => 'FAQ Categories',
        'singular_name'              => 'FAQ Category',
        'menu_name'                  => 'FAQ Categories',
        'all_items'                  => 'All FAQ Categories',
        'parent_item'                => 'FAQ Category',
        'parent_item_colon'          => 'FAQ Category:',
        'new_item_name'              => 'New FAQ Category',
        'add_new_item'               => 'Add New FAQ Category',
        'edit_item'                  => 'Edit FAQ Category',
        'update_item'                => 'Update FAQ Category',
        'separate_items_with_commas' => 'Separate FAQ Category with Commas',
        'search_items'               => 'Search FAQ Categories',
        'add_or_remove_items'        => 'Add or Remove FAQ Categories',
        'choose_from_most_used'      => 'Choose from the most used FAQ categories',
    );
    $rewrite = array(
        'slug'                       => 'faqs/category',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => $rewrite,
    );
    //register_taxonomy( 'test_faqs_category', 'test_faqs', $args );
  }

} new custom_taxonomies ;