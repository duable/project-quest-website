<?php

namespace du;

class theme_support {

  function __construct() {
    add_action( 'after_setup_theme', array( $this, 'features' ) );
    add_action( 'after_setup_theme', array( $this, 'deactivate' ) );
    add_action( 'widgets_init', array( $this, 'remove_recent_comment_style' ) );
  }

  public function features() {

    /* Post Formats */
    add_theme_support( 'post-formats', array(
      'aside',
      'gallery',
      'video',
      'quote' 
      )
    );

    /* Some nice-to-haves */
    add_editor_style();
    add_theme_support( 'automatic-feed-links' );
    add_post_type_support( 'page', 'excerpt' );

    /* Post Thumbnails */
    add_theme_support( 'post-thumbnails' );
    //add_image_size( 'banner-slider', 1000, 600, true );
    //add_image_size( 'small-wide', 750, 450, true );

    /* HTML5 Support */
    add_theme_support( 'html5', array( 
      'comment-list', 
      'comment-form', 
      'search-form', 
      'gallery', 
      'caption'  
      ) 
    );

    /* Custom Header */
    $header_defaults = array(
      'default-image'          => '',
      'random-default'         => false,
      'width'                  => 1600,
      'height'                 => 400,
      'flex-height'            => false,
      'flex-width'             => false,
      'default-text-color'     => '',
      'header-text'            => false,
      'uploads'                => true,
      'wp-head-callback'       => '',
      'admin-head-callback'    => '',
      'admin-preview-callback' => '',
    );

    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
    add_theme_support( 'custom-logo', array(
      'height' => 70,
      'width' => 350,
      'flex-height' => true,
      'flex-width' => true,
    ) );

    load_theme_textdomain( 'elementor-hello-theme', get_template_directory() . '/languages' );
    //add_theme_support( 'custom-header', $header_defaults );
  }

  public function deactivate() {
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'noindex', 1);

    # Remove emoji dependencies and support
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
    remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
  }

  public function remove_recent_comment_style() {
    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }

} new theme_support;