<?php

namespace du;

use MatthiasMullie\Minify;

class scripts_styles {

  #  Load Scripts
  # 'name-for-script' => '/path/relative/to/theme/file.js'
  public $javascript_files = array(
    'polyfills'       => '/assets/js/polyfills.js',
    'headroom'        => '/assets/js/headroom.js',
    'global'          => '/assets/js/global.js'
  );

  #  Load Stylesheets
  # 'name-for-script' => '/path/relative/to/theme/file.css'
  public $css_files = array(
    'modules'       => '/assets/css/modules.css',
    'base'          => '/assets/css/base.css',
  );

  # Toggles Minifying
  public $minify_js     = false;
  public $minify_styles = false;

  function __construct() {

    # Minified File Locations
    $this->mini_javascript  = '/assets/js/global.min.js';
    $this->mini_css         = '/assets/css/base.min.css';

    add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'load_styles' ), 100 );
    add_action( 'wp_enqueue_scripts', array( $this, 'conditional_js' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'conditional_css' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
  }

  function conditional_js() {

    # Load Ajax Script
    wp_localize_script( 
      'global', 
      'the_ajax_script', 
      array( 
        'ajax_url'  => admin_url( 'admin-ajax.php' ),
        'nonce'     => wp_create_nonce( 'duable' )
      ) 
    );
  }

  function conditional_css() {

    /*
      global $post;
      if ( is_page( $post->ID ) ) {
        wp_enqueue_style( 'du-local', du_site_asset( '/assets/css/pages.css' ), 'du-theme' );
      } 
    */
  }

  function admin_scripts() {}

  /**
   * ***************
   * LOAD JAVASCRIPT
   * ***************
   */
  function load_scripts() {
    # Filter for manipulating file list
    $this->javascript_files = apply_filters( 'du_js_files', $this->javascript_files );
    $this->load_full_js();
    /**
     * Create a timestamp variable for each javascript file
     */
    // $this->mini_javascript_timestamp = ( file_exists( get_stylesheet_directory() . $this->mini_javascript ) ? filemtime( get_stylesheet_directory() . $this->mini_javascript ) : 0 );

    // foreach ( $this->javascript_files as $script => $path ) :
    //   $script_timestamp = ( file_exists( get_stylesheet_directory() . $path ) ? filemtime( get_stylesheet_directory() . $path ) : 0 );

      /**
       * If any of the js files is newer than the minified script then 
       * let's load the full versions and end the foreach loop
       */
      // if ( $script_timestamp > $this->mini_javascript_timestamp && $this->minify_js == true ) :
      //   $this->minify_js();
      // endif;

      /**
       * Check if we are on the production server.  If we are
       * then let's load the minified version of assets.
       */
    //   if ( $this->minify_js == true ) :
    //     $this->load_mini_js();
    //   else :
    //     $this->load_full_js();
    //   endif;

    // endforeach;
  }

  function minify_js() {
    $first_file = true;
    foreach ( $this->javascript_files as $script => $path ) {
      if ( $first_file == true ) :
        $minifier = new Minify\JS( get_stylesheet_directory() . $path );
      else : 
        $minifier->add( get_stylesheet_directory() . $path );
      endif;
      $first_file = false;
    }
    $minified = $minifier->minify();
    $destination = get_stylesheet_directory() . $this->mini_javascript;
    file_put_contents( $destination, $minified );
  }

  function load_mini_js() {
    wp_register_script(
      'global'
      , ( du_site_asset( $this->mini_javascript ) )
      , 'jquery'
      , null
      , true
    );
    wp_enqueue_script( 'global' );
  }

  function load_full_js() {
    foreach ( $this->javascript_files as $script => $path ) {
      wp_register_script(
        $script
        , ( du_site_asset( $path ) )
        , 'jquery'
        , null
        , true 
      );
      wp_enqueue_script( $script ); 
    }
  }

  /**
   * ****************
   * LOAD STYLESHEETS
   * ****************
   */
  function load_styles() {
    # Filter for manipulating file list
    $this->css_files = apply_filters( 'du_css_files', $this->css_files );
    $this->load_full_css();
    /**
     * Create a timestamp variable for each css file
     */
    // $this->mini_css_timestamp = ( file_exists( get_stylesheet_directory() . $this->mini_css ) ? filemtime( get_stylesheet_directory() . $this->mini_css ) : 0 );

    // foreach ( $this->css_files as $script => $path ) {
    //   $script_timestamp = ( file_exists( get_stylesheet_directory() . $path ) ? filemtime( get_stylesheet_directory() . $path ) : 0 );

      /**
       * If any of the js files is newer than the minified script then 
       * let's load the full versions and end the foreach loop
       */
      // if ( $script_timestamp > $this->mini_css_timestamp && $this->minify_styles == true  ) :
      //   $this->minify_css();
      // endif;
      
      /**
       * Check if we are on the production server.  If we are
       * then let's load the minified version of assets.
       */
      // if ( $this->minify_styles == true ) :
      //   $this->load_mini_css();
      // else :
      //   $this->load_full_css();
      // endif;
    // }
  }

  function minify_css(){ 
    $first_file = true;
    foreach ( $this->css_files as $script => $path ) {
      if ( $first_file == true ) :
        $minifier = new Minify\CSS( get_stylesheet_directory() . $path );
      else : 
        $minifier->add( get_stylesheet_directory() . $path );
      endif;
      $first_file = false;
    }
    $minified = $minifier->minify();
    $destination = get_stylesheet_directory() . $this->mini_css;
    file_put_contents($destination, $minified);
  }

  function load_mini_css() {
    wp_enqueue_style( 'du-base', du_site_asset( $this->mini_css ), array(), null );
  }

  function load_full_css() {
    foreach ( $this->css_files as $script => $path ) {
      wp_enqueue_style( $script, du_site_asset( $path ), array(), null );
    }
  }

} new scripts_styles;