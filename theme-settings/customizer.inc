<?php

class du_customizer {

  function __construct() {
    #add_action( 'customize_register', array( $this, 'customize_register' ) );
  }

  function customize_register( $wp_customize ) {
    $wp_customize->add_setting( 'header_textcolor' , array(
        'default'     => '#000000',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_section( 'mytheme_new_section_name' , array(
        'title'      => __( 'Visible Section Name', 'mytheme' ),
        'priority'   => 30,
    ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
      'label'        => __( 'Header Color', 'mytheme' ),
      'section'    => 'mytheme_new_section_name',
      'settings'   => 'header_textcolor',
    ) ) );
     //All our sections, settings, and controls will be added here
  } 

} new du_customizer;