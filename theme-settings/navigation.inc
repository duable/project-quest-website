<?php

namespace du;

class navigation {

  function __construct() {
    add_action( 'init', array( $this, 'register_menus' ) );
  }

  public function register_menus() {
    register_nav_menu( 'header-navigation' ,__( 'Header Navigation' ) );
    register_nav_menu( 'footer-navigation' ,__( 'Footer Navigation' ) );
  }

} new navigation;
