<?php
/**
 * Footer Template
 * 
 * @category   Footer
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */
?>

<?php
if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) {
	get_template_part( 'templates/footer' );
}
?>

<?php wp_footer(); ?>

</body>
</html>