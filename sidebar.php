<?php
/**
 * Sidebar
 * 
 * @category   Sidebar
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */
?>

<section class="main-sidebar sidebar">
  <?php 
    if ( is_active_sidebar( 'main-sidebar' ) )
      dynamic_sidebar( 'main-sidebar' );
  ?>
</section>