<?php
/**
 * Functions
 * 
 * @category   Functions
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */

class du_load_theme {

  function __construct() {
    # Define constants
    $this->definitions();

    # Load API keys before dependencies
    $this->load_api_keys();

    # Load composer vendor autoloads
    $this->autoload_vendors();

    # Load theme settings
    $this->load_settings();

    # Load theme classes
    $this->load_classes();
  }

  function definitions() {
    # Define directory location
    define( 'DU_VENDOR_DIR', get_stylesheet_directory() . '/vendor' );
    define( 'DU_VENDOR_URI', get_stylesheet_directory_uri() . '/vendor' );

    # Theme settings dir
    define( 'DU_SETTINGS_DIR', get_stylesheet_directory() . '/theme-settings' );

    # User's Full Name
    if ( is_user_logged_in() ) {
      $user = get_user_by( 'id', get_current_user_id() );
      define( 'USER_FULL_NAME', $user->first_name . ' ' . $user->last_name );
    }
  }

  function autoload_vendors() {
    # Autoload
    $autoload_script = DU_VENDOR_DIR . '/autoload.php';
    if ( file_exists( $autoload_script ) )
      include_once $autoload_script;
  }

  function load_api_keys() {
    include_once __DIR__ . '/theme-settings/api_auth.inc' ;
  }

  function load_settings() {
    # Include theme options
    foreach ( glob( __DIR__ . '/theme-settings/*.inc' ) as $file ) 
      include_once $file ;
  }

  function load_classes() {
    # Include theme classes
    foreach ( glob( __DIR__ . '/theme-classes/*.php' ) as $file ) 
      include_once $file ;
  }

} new du_load_theme;