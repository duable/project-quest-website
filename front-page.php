<?php
/**
 * Front Page
 * 
 * @category   Page
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */

get_header(); ?>

<main id="body" role="main" class="home">

<?php while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

<?php endwhile; ?>

</main>

<?php get_footer(); ?>