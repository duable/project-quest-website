<?php
/**
 * Home Post Archive
 * 
 * @category   Page
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */ 
get_header();?>

<main id="body" class="blog-listing right-sidebar">

  <div class="container">

    <article class="main-content content copy">
      
      <header class="page-title">
        <h1><?php echo du_page_title( get_option( 'page_for_posts' ) ); ?></h1>
      </header>

      <div class="int-content">

        <section class="listing-items">
        <?php 
          while ( have_posts() ) : the_post();
        ?>
          <div <?php post_class( 'item' ); ?>>
            <?php get_template_part( 'content', 'listing' ); ?>
          </div>
        <?php
          endwhile;
          du_paging_nav();
        ?>
        </section>

      </div>

    </article>

    <?php get_sidebar(); ?>

  </div>


</main>

<?php get_footer(); ?>
