<?php

namespace pquest;

global $_events;

$_events = new events;

class events {
	
	public function __construct() {
		# Initialize events Post Type
		add_action( 'init', array( $this, 'post_type' ) );

		# Initialize events Category Taxonomy
		add_action( 'init', array( $this, 'taxonomies' ) );
	}


	public function post_type() {
	
		$labels = array(
		    'name'                => 'Events',
		    'singular_name'       => 'Events',
		    'menu_name'           => 'Events',
		    'parent_item_colon'   => 'Parent Events:',
		    'all_items'           => 'All Events',
		    'view_item'           => 'View Events',
		    'add_new_item'        => 'Add Events',
		    'add_new'             => 'New Events',
		    'edit_item'           => 'Edit Events',
		    'update_item'         => 'Update Events',
		    'search_items'        => 'Search Events',
		    'not_found'           => 'No Events found',
		    'not_found_in_trash'  => 'No Events found in trash',
		);

		$args = array(
			'labels'            => $labels,
			'has_archive' 			=> true,
			'public' 						=> true,
			'hierarchical' 			=> false,
			'supports' 					=> array(
				'title',
				'editor',
				'custom-fields',
				'thumbnail',
			),
			'rewrite'   				=> array( 
				'slug' 						=> 'events'
			),
			'menu_icon'        	=> 'dashicons-calendar',
		);

    register_post_type( 'events', $args );

  }

  static function taxonomies() {
    /**
     * Module Types Taxonomy
     */
    $labels = array(
        'name'                       => 'Event Category',
        'singular_name'              => 'Event Category',
        'menu_name'                  => 'Event Categories',
        'all_items'                  => 'All Event Categories',
        'parent_item'                => 'Event Category',
        'parent_item_colon'          => 'Event Category:',
        'new_item_name'              => 'New Event Category',
        'add_new_item'               => 'Add New Event Category',
        'edit_item'                  => 'Edit Event Category',
        'update_item'                => 'Update Event Category',
        'separate_items_with_commas' => 'Separate Event Categories with Commas',
        'search_items'               => 'Search Event Categories',
        'add_or_remove_items'        => 'Add or Remove Event Categories',
        'choose_from_most_used'      => 'Choose from the most used Event Categories',
    );
    $rewrite = array(
        'slug'                       => 'event-categories',
        'with_front'                 => false,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'event_categories', array( 'events' ), $args );
  }
}