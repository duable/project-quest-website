<?php

namespace pquest;

global $_framed_link;

//$_framed_link = new framed_link;


class framed_link {

	public function __construct() {
		add_action( 'wp_body_open', function() {
			if ( !empty( $_GET[ 'framed_link' ] ) ) :
				$this->get_form( $_GET[ 'framed_link' ] );
			endif;
		});

		add_action( 'wp_footer', array( $this, 'link_redirects' ) );
	}

	public function link_redirects() {
?>
<script>
document.addEventListener(`click`, e => {
  const origin = e.target.closest("a");
  
  if ( origin ) {
  	let target_url = origin.href;
  	if ( target_url.includes( ".kindful.com" ) ) {
  		target_url.endsWith('/') ? target_url.slice(0, -1) : target_url;
  		let ea_slug = target_url.split("/").pop();
  		e.preventDefault();
  		window.location.href = "<?php echo home_url(); ?>/?framed_link=" + encodeURIComponent( target_url );
  	}
  }
});
</script>
<?php
	}

	public function get_form( $link = null ) {

		if ( empty( $link ) ) $link = $_GET[ 'framed_link' ];
		?>
		<style>
			.ea-form-frame{
				position: absolute;
				left:  0;
				right:  0;
				top:  25px;
				width:  100vw;
				height:  calc( 100vh - 25px );
				box-sizing: border-box;
				border: none;
				overflow:  auto;
			}
			/*.elementor-3104 .elementor-element.elementor-element-5c6d5262:not(.elementor-motion-effects-element-type-background), .elementor-3104 .elementor-element.elementor-element-5c6d5262 > .elementor-motion-effects-container > .elementor-motion-effects-layer{
				background-color: transparent;
				background-image: linear-gradient(10deg, var( --e-global-color-secondary ) 0%, #283C86 85%);
				opacity: 1;
				transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
			}*/
		</style>
		<iframe class="ea-form-frame" src="<?php echo $link; ?>"></iframe>

<?php wp_footer(); ?>

</body>
</html>
		<?php
		die();
	}
	
}