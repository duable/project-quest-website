<?php

namespace pquest;

global $_news;

$_news = new news;

class news {
	
	public function __construct() {
		# Initialize News Post Type
		add_action( 'init', array( $this, 'post_type' ) );

		# Initialize News Category Taxonomy
		add_action( 'init', array( $this, 'taxonomies' ) );

		# Permalink to article URL
		add_filter( 'post_type_link', array( $this, 'edit_permalinks' ), 1, 3 );
	}



	public function edit_permalinks( $link, $post ) {
		if ( !empty( $post->ID ) && get_post_type( $post->ID ) === 'news'  ) :
			$link = get_field( 'article_link', $post->ID );
		endif;
	
		return $link;
  	}


	public function post_type() {
	
		$labels = array(
		    'name'                => 'News',
		    'singular_name'       => 'News',
		    'menu_name'           => 'News',
		    'parent_item_colon'   => 'Parent News:',
		    'all_items'           => 'All News',
		    'view_item'           => 'View News',
		    'add_new_item'        => 'Add News',
		    'add_new'             => 'New News',
		    'edit_item'           => 'Edit News',
		    'update_item'         => 'Update News',
		    'search_items'        => 'Search News',
		    'not_found'           => 'No News found',
		    'not_found_in_trash'  => 'No News found in trash',
		);

		$args = array(
			'labels'            => $labels,
			'has_archive' 			=> true,
			'public' 						=> true,
			'hierarchical' 			=> false,
			'supports' 					=> array(
				'title',
				'editor',
				'custom-fields',
				'thumbnail',
			),
			'rewrite'   				=> array( 
				'slug' 						=> 'news'
			),
			'menu_icon'        	=> 'dashicons-media-document',
		);

    register_post_type( 'news', $args );

  }

  static function taxonomies() {
    /**
     * Module Types Taxonomy
     */
    $labels = array(
        'name'                       => 'News Category',
        'singular_name'              => 'News Category',
        'menu_name'                  => 'News Categories',
        'all_items'                  => 'All News Categories',
        'parent_item'                => 'News Category',
        'parent_item_colon'          => 'News Category:',
        'new_item_name'              => 'New News Category',
        'add_new_item'               => 'Add New News Category',
        'edit_item'                  => 'Edit News Category',
        'update_item'                => 'Update News Category',
        'separate_items_with_commas' => 'Separate News Categories with Commas',
        'search_items'               => 'Search News Categories',
        'add_or_remove_items'        => 'Add or Remove News Categories',
        'choose_from_most_used'      => 'Choose from the most used News Categories',
    );
    $rewrite = array(
        'slug'                       => 'news-categories',
        'with_front'                 => false,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'news_categories', array( 'news' ), $args );
  }
}