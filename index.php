<?php
/**
 * Default Fallback Page Layout
 * 
 * @category   Page
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */

get_header();

$is_elementor_theme_exist = function_exists( 'elementor_theme_do_location' );
if ( is_singular() ) {
  if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
    get_template_part( 'templates/single' );
  }
} elseif ( is_archive() || is_home() || is_search() ) {
  if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'archive' ) ) {
    get_template_part( 'templates/archive' );
  }
} else {
  if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
    get_template_part( 'templates/404' );
  }
}

get_footer();