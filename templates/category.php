<?php
/**
 * Category Listing Layout
 * 
 * @category   Category
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */

/* Get the category object */
$categories = get_the_category();

get_header(); ?>

<main role="main" id="body" class="category-listing right-sidebar">

  <div class="container">

    <article class="main-content content copy">

      <header class="page-title">
        <h1 class="post-category <?php echo $categories[0]->slug; ?>">
          <?php printf( __( '%s', 'du_base' ), single_cat_title( '', false ) ); ?>
        </h1>
			</header>

      <?php
        $term_description = term_description();
        if ( ! empty( $term_description ) ) :
          printf( '<div class="taxonomy-description">%s</div>', $term_description );
        endif;
      ?>

      <?php 
        if ( have_posts() ) : 
      ?>
      
        <?php 
          if ( is_active_sidebar( 'category-above-content' ) ) : 
        ?>
          <div class="category-above-content-widgets">
            <?php dynamic_sidebar( 'category-above-content' ); ?>
          </div>
        <?php 
          endif; 
        ?>

        <div class="int-content">

          <section class="listing-items" data-columns>
          <?php 
            while ( have_posts() ) : the_post();
          ?>
            <div <?php post_class( 'item' ); ?> data-column="one-third">
              <?php get_template_part( 'content', 'listing' ); ?>
            </div>
          <?php
      			endwhile;
            du_paging_nav();
          ?>
          </section>

        </div>
  
        <?php 
          if ( is_active_sidebar( 'category-below-content' ) ) : 
        ?>

          <div class="category-below-content-widgets">
            <?php dynamic_sidebar( 'category-below-content' ); ?>
          </div>

        <?php 
          endif; 
        ?>

			<?php 
        endif; 
      ?>

    </article>

    <?php 
      get_sidebar( 'category' ); 
    ?>

  </div>

</main>

<?php get_footer(); ?>
