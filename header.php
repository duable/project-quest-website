<?php
/**
 * Header
 * 
 * @category   Header
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */
?>
<!doctype html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php wp_title( '|' ); ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class( '' ); ?>>

<?php
if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'header' ) ) {
  get_template_part( 'templates/header' );
}

wp_body_open();