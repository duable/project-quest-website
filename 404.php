<?php
/**
 * 404 LAYOUT
 * 
 * @category   Page
 * @package    du_theme
 * @subpackage WordPress
 * @author     Duable <dev@duable.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0
 * @link       http://src.duable.com/du-theme
 */

get_header();  ?>

<main id="body" class="error-404 right-sidebar">

  <div class="container">

    <article class="main-content content copy">
      
      <header class="page-title">
        <h1>404 Error</h1>
      </header>

      <div class="int-content">
        <h2>The page you are looking for was not found.</h2>
      </div>
      
    </article>

    <?php 
      get_sidebar( 'page' ); 
    ?>
  
  </div>
  
</main>

<?php get_footer(); ?>
